#! /bin/env python3

"""
	WikiPath.py ходит по ссылкам Википедии
"""

import argparse, os, random, re
import aiohttp, asyncio, async_timeout, uvloop
from urllib.parse import quote
from yarl import URL, urlunsplit
from bs4 import BeautifulSoup as bs

__version__ = '0.9.4'

parser = argparse.ArgumentParser(description='Скрипт заходит на вики-страницу и выбирает случайную ссылку и переходит на неё')
aa = parser.add_argument
aa('-U', '--url', default='wikipedia.org', help='Сменить адрес')
aa('-L', '--lang', default='ru', help='Сменить язык у вики')
aa('-NL', '--notlang', action='store_true', default=False, help='Если вики-сайт не поддерживает языковые домены')
aa('-P', '--page', default='Лысьва', help='Сменить начальную страницу')
aa('-C', '--count', default=10,  type=int, help='Количество переходов')
aa('-1', '--firstlink', action='store_true', default=False, help='Переходит только по первым ссылкам')
args = parser.parse_args()

count = args.count

async def fetch(session, url):
	with async_timeout.timeout(10):
		async with session.get(url) as response:
			return await response.text()

async def main(loop, url):
	async with aiohttp.ClientSession(loop=loop) as session:
		html = await fetch(session, url)
		return html
        
random.seed(os.urandom(20))
asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
loop = asyncio.get_event_loop()
l = loop.run_until_complete
def getLinks(articleUrl):
	url = urlunsplit(('https', args.lang+'.'+args.url, articleUrl, '', ''))
	
	bsObj = bs(l(main(loop, url)), 'html5lib')
	bsBodyContent = bsObj.find('div', {'id': 'bodyContent'})
	
	[cont.extract() for cont in bsBodyContent('table', class_=re.compile(r'infobox|vertical-navbox|ambox|wikidict-box'))]
	[cont.extract() for cont in bsBodyContent('div', class_=re.compile(r'infobox|wikidict-box'))]
	[cont.extract() for cont in bsBodyContent('div', class_='hatnote')]
	[cont.extract() for cont in bsBodyContent('div', class_='magnify')]
	[cont.extract() for cont in bsBodyContent('div', class_='thumb tright')]
	[cont.extract() for cont in bsBodyContent('div', class_='dablink')]
	[cont.extract() for cont in bsBodyContent('a', class_='mw-disambig')]
		
	if args.firstlink:
		s = str(bsBodyContent)
		s = re.sub(r'\s\(.*?\)', '', s)
		bsBodyContent = bs(s, 'html5lib')
		
	return bsBodyContent.findAll('a', href=re.compile('^(/wiki/)((?!:).)+$')), URL(url)

if __name__ == '__main__':
	links, url = getLinks('/wiki/'+quote(args.page))
	try:
		first_links = set({'/wiki/'+quote(args.page)})
		for i in range(1, count+1):
			newArticle = links[0].attrs['href'] if args.firstlink else links[random.randint(0, len(links)-1)].attrs['href']
			print(str(i)+'.  ', url.human_repr())
			if args.firstlink:
				y = 0
				while newArticle in first_links:
					y += 1
					newArticle = links[y].attrs['href']
				first_links.add(newArticle)
				if url.name in ('Philosophy', 'Философия'):
					print('Бинго!')
					break
			links, url = getLinks(newArticle)

	except KeyboardInterrupt:
		print('--Отменено--')
	except IndexError:
		print('--В статье не найдены ссылки--')
